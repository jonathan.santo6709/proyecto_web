<br>
<center>
  <h2>LISTADO DE CLIENTES</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/clientes/nuevo" class="btn btn-info"><i class="fa fa-plus-circle"></i> Agregar Usuario</a>
</center>

<?php if ($listadoClientes): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table table-bordered table-striped table-hover" id="tbl-clientes">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">FOTO</th>
        <th class="text-center">IDENTIFICACION</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">ESTADO</th>
        <th class="text-center">DIRECCION</th>
        <th class="text-center">TELEFONO</th>
        <th class="text-center">OPCIONES</th>


      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoClientes->result() as $filaTemporal):  ?>
        <tr>
          <td class="text-center">
            <?php echo $filaTemporal->id_cli; ?>
          </td>
          <!--PARA PONER VISUALIZAR LAS IMAGENES EN EL FORMULARIO DEL INDEX-->
          <td class="text-center">
                  <?php if ($filaTemporal->foto_cli!=""): ?>
                    <img src="<?php echo base_url(); ?>/uploads/clientes/<?php echo $filaTemporal->foto_cli; ?>"
                    height="80px"
                    width="100px"
                    alt="">
                  <?php else: ?>
                    N/A
                  <?php endif; ?>
                </td>

          <td class="text-center">
            <?php echo $filaTemporal->identificacion_cli; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->apellido_cli; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->nombre_cli; ?>
          </td>
          <td class="text-center">
            <?php if ($filaTemporal->estado_cli=="ACTIVO"): ?>
    <div class="alert alert-success">
      <?php echo $filaTemporal->estado_cli; ?>
    </div>
  <?php else: ?>
    <div class="alert alert-danger">
      <?php  echo $filaTemporal->estado_cli ?>
    </div>
  <?php endif; ?>


          <td class="text-center">
            <?php echo $filaTemporal->direccion_cli; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->telefono_cli; ?>
          </td>

          <td class="text-center">

            <a class="btn btn-success"  href="<?php echo site_url(); ?>/clientes/editar/<?php echo $filaTemporal->id_cli; ?>" > <i class="fa fa-edit"></i></a>

          <?php   if ($this->session->userdata('c0nectadoUTC')->perfil_usu=='ADMINISTRADOR'):  ?>
          <a  href='javascript:void(0)'
          onclick="confirmarEliminacion('<?php echo$filaTemporal->id_cli; ?>');"
            class="btn btn-danger">
            <i class="fa fa-trash"></i>
          </a>
        <?php endif; ?>
          </td>
        </tr>
  <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON CLIENTES REGISTRADOS</h1>
  </div>
<?php endif; ?>

<script type="text/javascript">
  function confirmarEliminacion(id_cli){
    iziToast.question({
    timeout: 10000,
    close: false,
    overlay: true,
    displayMode: 'once',
    id: 'question',
    zindex: 999,
    title: 'CONFIRMACION',
    message: 'ESTAS SEGURO DE ELIMINAR',
    position: 'center',
    buttons: [
        ['<button><b>SI BB</b></button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
            window.location.href="<?php echo site_url(); ?>/clientes/procesarEliminacion/"+ id_cli;

        }, true],
        ['<button>NO BB</button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

        }],
    ]
});

  }

</script>


<script type="text/javascript">
//YA ESTA EN ESPAÑOL
$(document).ready(function() {
    $("#tbl-clientes").DataTable( {
        language: {
            url: 'https://cdn.datatables.net/plug-ins/1.12.1/i18n/es-ES.json'
        }
    } );
} );



</script>
<script type="text/javascript">
    function confirmarEliminacion(ide_usu){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el cliente de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/clientes/procesarEliminacion/"+ide_usu;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
