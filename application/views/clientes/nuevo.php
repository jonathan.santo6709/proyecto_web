<br>
<h1> <center>Registro de clientes</center> </h1>
<!--COLUMNA 1 enctype="multipart/form-data" agregar para qeu se suban los archivos-->
<form action="<?php echo site_url(); ?>/clientes/guardarCliente" method="post" id="frm_nuevo_cliente" enctype="multipart/form-data">

  <!--COLUMNA 1 -->
    <br>
    </select>
    <br>
    <b>IDENTIFICACION: </b>
    <br>
    <input type="number" class="form-control" name="identificacion_cli" id="identificacion_cli" value="" placeholder="Por favor ingrese la identificacion" class="form-control input-sm " required>
    <br>
    <b>NOMBRE: </b>
    <br>
    <input type="text" class="form-control" name="nombre_cli" id= "nombre_cli"value="" placeholder="Ingrese su nombre" class="form-control input-sm " required>
    <br>
    <b>APELLIDO: </b>
    <br>
    <input type="text" class="form-control" name="apellido_cli" id= "apellido_cli"value="" placeholder="Ingrese el apellido" class="form-control input-sm " required>
    <br>
    <b>TELEFONO: </b>
    <br>
    <input type="number" class="form-control"  name="telefono_cli" id= "telefono_cli"value="" placeholder="Ingrese su numero de telefono" class="form-control input-sm " required>
    <br>
    <b>EMAIL: </b>
    <br>
    <input type="email" class="form-control" name="email_cli" id= "email_cli"value="" placeholder="Ingrese su email" class="form-control input-sm " required>
    <br>
    <b>DIRECCION: </b>
    <br>
    <input type="text" class="form-control" name="direccion_cli" id= "direccion_cli"value="" placeholder="Ingrese su direccion" class="form-control input-sm " required>
    <br>

    <div class="row">
              <div class="col-md-12">
                <b><label for="">ESTADO:</label></b>
                  <select class"form-control" type="text" name="estado_cli" id="estado_cli" class="form-control input-sm " required> <option value="">SELECCIONE PORFAVOR</option>
                    <option value"MATRICULADO">ACTIVO</option>
                    <option value="PENDIENTE">PENDIENTE</option>

                  </select>
              </div>
            </div>


    <!--para crear y poner una foto accept para que unicamente seleccione imagenes-->
    <br>
    <br>
    <b>FOTOGRAFIA: </b>
    <input type="file" name="foto_cli" accept="image/*" id='foto_cli' value="">

    <br>
    <br>
    <!--hasta aqui antes del boton guardar -->
    <button type="submit" name="button"  class="btn btn-primary">GUARDAR</a></button>
    <!--PARA DAR ESPACICOS HACIA LA DERECHA-->
    &nbsp;&nbsp;&nbsp;
    <button type="button" name="button"><a href="<?php echo site_url(); ?>/clientes/index" class="btn btn-warning"><i class="fa solid fa-ban"></i> CANCELAR</a></button>

</form>

<script type="text/javascript">
    $("#frm_nuevo_cliente").validate({
      rules:{
        fk_id_pais:{
          required:true
        },
        identificacion_cli:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        nombre_cli:{
          letras:true,
          required:true
        },
        apellido_cli:{
          letras:true,
          required:true
        },
        telefono_cli:{
          required:true
        },
        email_cli:{
          email:true,
          required:true
        },
        direccion_cli:{
          required:true
        },
        estado_cli:{
          required:true
        }
      },
      messages:{
        fk_id_pais:{
          required:"Por favor seleccione el pais"
        },
        identificacion_cli:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La cédula solo acepta números"
        },
        nombre_cli:{
          required:"Porfavor ingrese su nombre",
          letras:"Porfavor no ingrese numeros",
        },
        apellido_cli:{
          required:"Porfavor ingrese su apellido",
          letras:"Porfavor no ingrese numeros",
        },
        telefono_cli:{
          required:"Porfavor ingrese su telefono",
          minlength:"El telefono debe tener mínimo 10 digitos",
          maxlength:"El telefono debe tener máximo 10 digitos",
          digits:"El telefono solo acepta números",
        },
        email_cli:{
          required:"Porfavor ingrese su email",
          email:"Email no valido utiliza un @ en el email precioso porfavor"
        },
        direccion_cli:{
          required:"Porfavor ingrese su direccion"
        },
        estado_cli:{
          required:"Porfavor ingrese su estado"
        }

      }
    });
</script>
<!--nueva importacion clase 29/6/2022-->
<script type="text/javascript">

  $('#foto_cli').fileinput({
    allowedFileExtensions:['jpeg','jpg','png'],
    dropZoneEnable:true,
    language:'es'

  });



</script>
