
  <title>Recuperar contraseña</title>
  <!-- base:css -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/vendors/typicons.font/font/typicons.css">
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url();?>/assets/images/utc.ico" />

  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <center>
                <img src="<?php echo base_url();?>/assets/images/utc.ico" alt="logo" >
                </center>
              </div>
              <center>
              <h4>Recuperar contraseña</h4>
              <h6 class="font-weight-light">Ingresar los datos para recuperar.</h6>
              </center>
              <form class="pt-3" action="<?php echo site_url("seguridades/recuperarPassword"); ?>" method="post">
                <div class="form-group">
                  <input type="email" name="email" class="form-control form-control-lg"  placeholder="Email">
                </div>
                <div class="mt-3">
                  <button class="btn btn-primary btn-lg btn-block" type="submit">RECUPERAR</button>
                </div>
                <div class="text-center mt-4 font-weight-light">
                  ¿Ya tienes una cuenta? <a href="<?php echo site_url(); ?>/seguridades/formularioLogin" class="text-primary">Iniciar Sesion</a>
                </div>
              </form>
              <?php if ($this->session->flashdata("error")): ?>
              <script type="text/javascript">
                  alert("<?php echo $this->session->flashdata("error"); ?> ");
              </script>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <?php if ($this->session->flashdata("error")):?>
  <script type="text/javascript">
  alert("<?php echo $this->session->flashdata("error");?>");
  </script>
  <?php endif;?>
