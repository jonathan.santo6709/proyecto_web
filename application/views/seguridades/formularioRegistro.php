
<title>Registro de usuario</title>
<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendors/typicons.font/font/typicons.css">
<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/vertical-layout-light/style.css">
<link rel="shortcut icon" href="<?php echo base_url();?>/assets/images/utc.ico" />

  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <center>
                <img src="<?php echo base_url();?>/assets/images/utc.ico" alt="logo" id="frm_nuevo_usuario" >
                </center>
              </div>
              <center>
              <h4>Bienvenido</h4>
              <h6 class="font-weight-light">Registra tu informacion para continuar.</h6>
              </center>
              <form class="pt-3" action="<?php echo site_url('usuarios/insertarUsuario');?>" method="post" id="frm_nuevo_usuario" enctype="multipart/form-data">
                <div class="form-group">
                  <input type="text" name="apellido_usu" id="apellido_usu" class="form-control form-control-lg"  placeholder="Apellido">
                </div>
                <div class="form-group">
                  <input type="text" name="nombre_usu" id="nombre_usu" class="form-control form-control-lg"  placeholder="Nombre">
                </div>
                <div class="form-group">
                  <input type="text" name="email_usu" id="email_usu" class="form-control form-control-lg" placeholder="Email">
                </div>
                <div class="form-group">
                  <input type="password" name="password_usu" id="password_usu" class="form-control form-control-lg"  placeholder="Contraseña">
                </div>
                <div class="form-group">
                  <input type="password" name="password_confirmada" id="password_confirmada" class="form-control form-control-lg"  placeholder="Confirma Contraseña">
                </div>
                <div class="mb-4">
                </div>
                <div class="mt-3">
                  <a class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" href="">INGRESAR</a>
                </div>
                <div class="text-center mt-4 font-weight-light">
                  ¿Ya tienes una cuenta? <a href="<?php echo site_url(); ?>/seguridades/formularioLogin" class="text-primary">Iniciar Sesion</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>

  <script type="text/javascript">
      $("#frm_nuevo_cliente").validate({
        rules:{
          nombre_usu:{
            letras:true,
            required:true
          },
          apellido_usu:{
            letras:true,
            required:true
          },
          email_usu:{
            email:true,
            required:true
          },
          password_usu:{
            required:true
          },
          password_confirmada:{
            required:true
          }
        },
        messages:{
          nombre_usu:{
            required:"Porfavor ingrese su nombre",
            letras:"Porfavor no ingrese numeros",
          },
          apellido_usu:{
            required:"Porfavor ingrese su apellido",
            letras:"Porfavor no ingrese numeros",
          },
          email_usu:{
            required:"Porfavor ingrese su email",
            email:"Email no valido utiliza un @ en el email precioso porfavor"
          },
          password_usu:{
            required:"Porfavor ingrese su contraseña"
          },
          password_confirmada:{
            required:"Porfavor ingrese su contraseña"
          }

        }
      });
  </script>

  <script type="text/javascript">

  jQuery.validator.addMethod("letras", function(value, element) {
       //return this.optional(element) || /^[a-z]+$/i.test(value);
       return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáé íñó]*$/.test(value);

     }, "Este campo solo acepta letras");

  </script>
